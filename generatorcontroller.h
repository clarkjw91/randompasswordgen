#ifndef GENERATORCONTROLLER_H
#define GENERATORCONTROLLER_H

#include <QObject>

class Generator;


enum eCharRangeSetter {
    ALPHA_ONLY = 0,
    ALPHA_NUMS,
    ALPHA_NUMS_SPEC
};

class GeneratorController : public QObject
{
    Q_OBJECT
public:
    GeneratorController();
    int passLengthToGui();

public slots:
    void genEvent();
    void getPasswordLength(int x);
    void getRangeSetterValue(int x);

signals:
    void passPasswordToGui(QString);
    void passLengthToGui(int);
private:

    Generator * gen;
    int currentPassLength;
    eCharRangeSetter currentRange;

};

#endif // GENERATORCONTROLLER_H
