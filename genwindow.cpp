#include "genwindow.h"
#include "generatebutton.h"
#include "generatorcontroller.h"

#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <QString>
#include <assert.h>

GenWindow::GenWindow(QWidget *parent) : QWidget(parent)
{
    srand(time(NULL));

    gen_control = new GeneratorController;
    assert(gen_control != 0);

//set window propperties
    setFixedSize(500, 350);
    //gui Objects
    title_lbl = new QLabel(this);
    assert(title_lbl != 0);
    title_lbl->setGeometry(10,10,480,50);
    title_lbl->setText("Random Password Generator");

   // int num = QInputDialog::getInt(this,"In","Out",0,3,20,1);
    pass_size_input = new QSpinBox(this);
    assert(pass_size_input != 0);
    pass_size_input->setGeometry(10,50,100,50);
    pass_size_input->setMinimum(gen_control->passLengthToGui());
    pass_size_input->setMaximum(50);
  //testing

    pass_type_input = new QComboBox(this);
    assert(pass_type_input != 0);
    pass_type_input->setGeometry(10, 140, 480, 50);
    pass_type_input->addItem("Letters Only", eCharRangeSetter::ALPHA_ONLY);
    pass_type_input->addItem("Letters and Numbers", eCharRangeSetter::ALPHA_NUMS);
    pass_type_input->addItem("Letters, Numbers, and Special Characters", eCharRangeSetter::ALPHA_NUMS_SPEC);


    gen_btn = new GenerateButton(this,10, 200);
    assert(gen_btn != 0);
    output_lbl = new QLabel(this);
    assert(output_lbl != 0);
    output_lbl->setGeometry(10, 300, 480, 50);


    //SIGNAL SLOT Connections
    connect(pass_size_input, SIGNAL(valueChanged(int)), gen_control, SLOT (getPasswordLength(int)));
    connect(pass_type_input, SIGNAL(currentIndexChanged(int)), gen_control, SLOT (getRangeSetterValue(int)));
    connect(gen_btn, SIGNAL (clicked()), gen_control, SLOT (genEvent()));

    connect(gen_control, SIGNAL(passPasswordToGui(QString)), output_lbl, SLOT (setText(QString)));
}

GenWindow::~GenWindow(){
    delete gen_control;
    delete gen_btn;
    delete pass_size_input;
    delete pass_type_input;
    delete output_lbl;
}
