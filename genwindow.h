#ifndef GENWINDOW_H
#define GENWINDOW_H

#include <QWidget>


//forward declarations
class QLabel;
class GenerateButton;
class Generator;
class GeneratorController;
class QSpinBox;
class QComboBox;

class GenWindow : public QWidget
{
    Q_OBJECT
public:
    explicit GenWindow(QWidget *parent = nullptr);
    ~GenWindow();
signals:

public slots:

private:
    QLabel * title_lbl;
    QLabel * output_lbl;
    QSpinBox * pass_size_input;
    QComboBox * pass_type_input;

    GenerateButton * gen_btn;
    GeneratorController * gen_control;

};

#endif // GENWINDOW_H
