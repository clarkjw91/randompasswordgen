#include "generatebutton.h"
class QWidget;


GenerateButton::GenerateButton(QWidget * parent, int xCoords, int yCoords)
    :QPushButton(parent)
{
    setGeometry(xCoords, yCoords, width, height);
    setText("Generate");

}
