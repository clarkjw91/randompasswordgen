#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include "genwindow.h"

int main(int argc, char **argv)
{
    QApplication app (argc, argv);
    GenWindow * window = new GenWindow();

    window->show();

    return app.exec();
}
