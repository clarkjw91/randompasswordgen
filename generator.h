#ifndef GENERATOR_H
#define GENERATOR_H

#include "generatorcontroller.h"
class Generator
{
public:
    Generator(int n);
    ~Generator();

    void genManager(eCharRangeSetter r);
    char* getPassword(){return pass;}
    int getPasswordLength(){return passSize;}

private:
    char * pass;
    const int passSize;

    void alphaGen();
    void alphaNumGen();
    void alphaNumSpecGen();
};

#endif // GENERATOR_H
