#include <math.h>
#include <stdlib.h>
#include <assert.h>

#include "generator.h"

Generator::Generator(int n) : passSize(n)
{
    pass = new char[n];
    assert(pass != 0);
}
Generator::~Generator()
{
delete[] pass;
}
void Generator::genManager(eCharRangeSetter r)
{
    switch(r){
        case ALPHA_ONLY :{
            alphaGen();
            break;
        }
        case ALPHA_NUMS : {
            alphaNumGen();
            break;
        }
        case ALPHA_NUMS_SPEC : {
            alphaNumSpecGen();
            break;
        }
    }

}

void Generator::alphaGen()
{
   bool upper, lower, validate = false;

   int r;
   do{
       lower = upper = false;
       for(int i = 0; i< passSize; i++){
           r = (rand() % 52) + 65;
           if(r > 90){//offset to match up with char dec values
               r += 6;
                lower = true;
           }else{
                upper = true;
           }
           pass[i] = r;
       }
       if(lower && upper)
           validate = true;
   }while(!validate);

}
void Generator::alphaNumGen()
{
    bool lower, upper, num, validate = false;
    int r = 0;
    do{
        lower = upper = num = false;
        for(int i = 0; i < passSize; i++){
            r = (rand() % 62) + 48;
            if(r <= 57){
                num = true;
            }
            else if(r > 57 && r <= 83){
                r += 7;
                lower = true;
            }else if(r > 87){
                r += 13;
                upper = true;
            }
            pass[i] = r;
        }
        if(num && lower && upper)
            validate = true;
    }while(!validate);
}
void Generator::alphaNumSpecGen()
{
    bool num, lower, upper, spec, validate = false;

    int r;
    do{
        num = lower = upper = spec = false;
        for(int i = 0; i < passSize; i++){
            r = (rand()% 93) + 33;
            if(r > 47 && r < 58)
                num = true;
            else if(r > 64 && r < 91)
                upper = true;
            else if (r > 96 && r < 123)
                lower = true;
            else if(r == 57 || r == 92){//remove ilegal characters, and amend counter
                i--;
                continue;
            }
            else
                spec = true;
            pass[i] = r;
        }
        if(num && lower && upper && spec)
            validate = true;
    }while(!validate);
}

