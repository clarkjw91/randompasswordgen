TEMPLATE = app
TARGET = RandomPasswordGen

QT = core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    main.cpp \
    genwindow.cpp \
    generatebutton.cpp \
    generator.cpp \
    generatorcontroller.cpp

HEADERS += \
    genwindow.h \
    generatebutton.h \
    generator.h \
    generatorcontroller.h
