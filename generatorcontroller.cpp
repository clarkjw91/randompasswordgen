#include <QString>
#include <assert.h>

#include "generatorcontroller.h"
#include "generator.h"


GeneratorController::GeneratorController()
{
    currentPassLength = 4;
    currentRange = ALPHA_ONLY;

}

void GeneratorController::genEvent()
{
    char * p;
    QString p_string;
    gen = new Generator(currentPassLength);
    assert(gen != 0);
    gen->genManager(currentRange);
    p = gen->getPassword();
    for(int i = 0; i < currentPassLength; i++){
        p_string += *p;
        p++;
    }
    emit passPasswordToGui(p_string);
    gen->~Generator();

}

void GeneratorController::getPasswordLength(int x){
   assert(x > 3);
    currentPassLength = x;
}
void GeneratorController::getRangeSetterValue(int x){
    currentRange = eCharRangeSetter(x);

}
int GeneratorController::passLengthToGui(){
    return currentPassLength;
}

