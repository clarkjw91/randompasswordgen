#ifndef GENERATEBUTTON_H
#define GENERATEBUTTON_H

#include <QPushButton>


class GenerateButton : public QPushButton
{
       Q_OBJECT
public:
   explicit GenerateButton(QWidget *parent, int xCoords, int yCoords);

signals:

public slots:

private:
    int width = 480;
    int height = 50;

};

#endif // GENERATEBUTTON_H
